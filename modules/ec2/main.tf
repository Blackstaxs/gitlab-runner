resource "aws_instance" "gitlab_runner" {
  ami           = var.ami_id  
  instance_type = var.instance_type 
  key_name      = var.key_name

  tags = {
    "Name"        = "${var.name_prefix}"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }
  
  user_data = file("${path.module}/script.sh")

}

